/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <enlapsed_time.h>

/*
 * Creates the folder for the timestamps
 */
static void mkdir_timestamps()
{
	struct stat st = {0};

	// Create only if doesn't already exist
	if (stat(DIR_TIMESTAMPS, &st) == -1)
		mkdir(DIR_TIMESTAMPS, 0700);
}

/*
 * Takes a folder and a file and returns a string with the junction of them
 * in the heap
 */
static char *concat_path(char *folder, char *file)
{
	int len_folder;
	int len_concat;
	int add_end_bar;
	char *concat;

	len_folder = strlen(folder);
	add_end_bar = folder[len_folder - 1] == '/' ? 0 : 1;
	len_concat = len_folder + add_end_bar + strlen(file) + 1;
	concat = malloc(len_concat);
	bzero(concat, len_concat);
	strcat(concat, folder);
	if (add_end_bar)
		strcat(concat, "/");
	strcat(concat, file);

	return (concat);
}

/*
 * Gets current epoch in milliseconds
 */
static t_millies get_millies()
{
	struct timespec tp = {0, 0};

	clock_gettime(CLOCK_REALTIME, &tp);
	return (tp.tv_sec * 1000 +	tp.tv_nsec / 1000000);
}

/*
 * This function processes a t_diff into a t_formatted_diff, dividing it
 * in hours, minutes, seconds and hundredths, acording to the options in ops
 */
static t_formatted_tdiff *parse_tdiff(t_millies diff, t_options *ops)
{
	t_formatted_tdiff *tdiff;

	tdiff = malloc(sizeof(t_formatted_tdiff));
	if (ops->maximum_unit == HOURS)
	{
		tdiff->hours = diff / 3600000;
		diff %= 3600000;
	}
	if (ops->maximum_unit >= MINUTES && ops->minimum_unit <= MINUTES)
	{
		tdiff->minutes = diff / 60000;
		diff %= 60000;
	}
	if (ops->maximum_unit >= SECONDS && ops->minimum_unit <= SECONDS)
	{
		tdiff->seconds = diff / 1000;
		diff %= 1000;
	}
	if (ops->minimum_unit == HUNDREDTHS)
		tdiff->hundredths = diff / 10;
	return (tdiff);
}

/*
 * This function displays to stdout a t_formatted_tdiff acording
 * to the options received in ops
 */
static void display_diff(t_formatted_tdiff *tdiff, t_options *ops)
{
	bool first_displayed = false;

	// Hours
	if (ops->maximum_unit == HOURS &&
			(tdiff->hours || ops->all_units))
	{
		printf("%u", tdiff->hours);
		first_displayed = true;
	}

	// Minutes
	if (ops->maximum_unit >= MINUTES && ops->minimum_unit <= MINUTES &&
			(tdiff->minutes || ops->all_units))
	{
		if (first_displayed)
			printf("%c", ops->separator);
		printf("%u", tdiff->minutes);
		first_displayed = true;
	}

	// Seconds
	if (ops->maximum_unit >= SECONDS && ops->minimum_unit <= SECONDS &&
			(tdiff->seconds || ops->all_units))
	{
		if (first_displayed)
			printf("%c", ops->separator);
		printf("%u", tdiff->seconds);
		first_displayed = true;
	}

	// Hundredths
	if (ops->minimum_unit == HUNDREDTHS)
	{
		if (first_displayed)
			printf("%c", ops->separator);
		printf("%02lu", tdiff->hundredths);
	}
}

/*
 * Read the timestamp from the specified filepath and returns it as t_millies
 */
static t_millies read_start_time(char *filepath)
{
	int fd;
	char buffer[15];
	t_millies ret;

	bzero(buffer, 15);
	if ((fd = open(filepath, O_RDONLY)) > 0)
	{
		read(fd, buffer, 14);
		close(fd);
	}

	// If it's the first time it reads that timestamp (read flag)
	ret = buffer[13] == 'a' ? strtoull(buffer, NULL, 10) : 0;
	return (ret);
}

/*
 * Overwrites the read flag to 'b' (read) in the specified filepath
 */
static void change_read_flag(char *filepath)
{
	int fd;

	if ((fd = open(filepath, O_WRONLY)) > 0)
	{
		lseek(fd, 13, SEEK_SET);
		write(fd, "b", 1);
		close(fd);
	}
}

/*
 * This function reads the "init" timestamp and displays the time difference
 * with the current moment. It will set the read flag in the timestamp unless
 * the option to not do so is active.
 */
static void comp(t_options *ops)
{
	t_millies diff;
	t_millies start_time;
	t_formatted_tdiff *tdiff;
	char *filepath;

	filepath = concat_path(DIR_TIMESTAMPS, ops->id);
	start_time = read_start_time(filepath);
	if (start_time)  // If the init timestamp exists
	{
		if (!ops->keep_timestamps)
			change_read_flag(filepath);
	}
	else
	{
		display_diff(&((t_formatted_tdiff){0, 0, 0, 0}), ops);
		free(filepath);
		return ;
	}
	free(filepath);

	diff = get_millies() - start_time;
	tdiff = parse_tdiff(diff, ops);
	display_diff(tdiff, ops);
	free(tdiff);
}

/*
 * This function creates or overwrites the timestamp file with the current
 * epoch. If the timestamps folder doesn't exists it creates it.
 */
static void init(t_options *ops)
{
	char current_epoch[15];
	char *filepath;
	int fd;

	mkdir_timestamps();

	filepath = concat_path(DIR_TIMESTAMPS, ops->id);
	if ((fd = open(filepath, O_WRONLY | O_CREAT, 0600)) > 0)
	{
		sprintf(current_epoch, "%llua", get_millies());
		write(fd, current_epoch, 14);
		close(fd);
	}
	free(filepath);
}

/*
 * I have no idea what this function is for /s
 */
int main(int argc, char *argv[])
{
	t_options *ops = parse_options(argc, argv);

	if (ops->mode == INIT)
		init(ops);
	else
		comp(ops);

	free_t_options(ops);
	return (EXIT_SUCCESS);
}
