DIR_TIMESTAMPS = /tmp/terms_timestamps/
SHELL = /bin/sh
CC = cc
CFLAGS = -Wall -Werror -Wextra -D DIR_TIMESTAMPS=\"$(DIR_TIMESTAMPS)\" -D PROG_NAME=\"$(NAME)\"
HEADER = -Iinclude
PREFIX = /usr/local
DEBUGGER = lldb

TARGETNAME = enlapsed_time
BINDIR = bin
OBJDIR = obj
SRCDIR = src
INCLUDEDIR = include
TARGET = $(BINDIR)/$(TARGETNAME)

OBJS:= obj/enlapsed_time.o \
	obj/parse_options.o \
	obj/retrieve_tty.o

SRCS:= src/enlapsed_time.c \
		src/parse_options.c \
		src/retrieve_tty.c

all: mkdirs $(TARGET)


$(TARGET): $(OBJS)
	$(CC) -o $@ $(OBJS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $(HEADER) -c $< -o $@ 

src/enlapsed_time.c: include/enlapsed_time.h
src/parse_options.c: include/enlapsed_time.h
src/retrieve_tty.c: include/enlapsed_time.h

obj/enlapsed_time.o: src/enlapsed_time.c
obj/parse_options.o: src/parse_options.c
obj/retrieve_tty.o:  src/retrieve_tty.c


mkdirs:
	mkdir -p $(BINDIR) $(OBJDIR)

clean:
	rm -rf $(BINDIR) $(OBJDIR)

install: $(TARGET)
	cp $(TARGET) $(PREFIX)/bin
	chmod +x $(PREFIX)/bin/$(TARGETNAME)

uninstall:
	rm $(PREFIX)/bin/$(TARGETNAME)

debug: CFLAGS += -g
debug: clean $(TARGET)
	$(DEBUGGER) $(TARGET)

.PHONY: all clean fclean re install uninstall debug
