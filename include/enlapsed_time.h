/*
 * Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ENLAPSED_TIME_H
#define ENLAPSED_TIME_H

#ifndef PROG_NAME
#define PROG_NAME "enlapsed_time"
#endif  // PROG_NAME

#ifndef DIR_TIMESTAMPS
#define DIR_TIMESTAMPS "/tmp/terms_timestamps/"
#endif  // DIR_TIMESTAMPS

#include <stdbool.h>

enum e_mode {INIT, COMP, UNSEL};
enum e_time_unit {HUNDREDTHS, SECONDS, MINUTES, HOURS};

typedef unsigned long long t_millies;

typedef struct s_formatted_tdiff
{
	unsigned long hundredths;
	unsigned int seconds;
	unsigned int minutes;
	unsigned int hours;
} t_formatted_tdiff;

typedef struct s_options
{
	enum e_mode mode;
	enum e_time_unit maximum_unit;
	enum e_time_unit minimum_unit;
	char separator;
	bool all_units;
	bool keep_timestamps;
	char *id;
} t_options;

char *retrieve_tty();
void free_t_options(t_options *ops);
t_options *parse_options(int argc, char *argv[]);

#endif  // ENLAPSED_TIME_H
